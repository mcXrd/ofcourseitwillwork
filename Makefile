isortchanges:
	sh/isortchanges.sh

generateproject:
	cookiecutter gh:Pylons/pyramid-cookiecutter-starter --checkout 1.9-branch

createvirtualenv:
	python3 -m venv env

upgradetools:
	env/bin/pip install --upgrade pip setuptools

installtestrequirements:
	env/bin/pip install -e ".[testing]"

runserver:
	env/bin/pserve development.ini

test:
	env/bin/pytest
