import unittest

import networkx as nx
from market_revisions_trader.services.trading_api.internal import (buy,
                                                                   can_buy,
                                                                   get_base_quote_digraph)
from pyramid import testing


class TradingApiTests(unittest.TestCase):

    def setUp(self):
        self.config = testing.setUp()

    def tearDown(self):
        testing.tearDown()

    def test_should_be_able_to_buy(self):
        assert can_buy('BNB', 'ADA') == ['BNB', 'ADA']

    def test_buy(self):
        return None
        # TODO to_usdt and from_usdt functions in binance_market.api
        b = buy(base='BNB', quote='ADA', quantity=0.1)
        assert b
        b = buy(base='ADA', quote='BNB', quantity=0.01)
        assert b

    def test_get_base_quote_digraph(self):
        g = get_base_quote_digraph()
        path = nx.shortest_path(g, source='ETH', target='BTC', weight='fee')
        assert g.edges[path[0], path[1]]['via'] == 'base_quote'
        path = nx.shortest_path(g, source='BTC', target='ETH', weight='fee')
        assert g.edges[path[0], path[1]]['via'] == 'quote_base'
