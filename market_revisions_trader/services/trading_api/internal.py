"""
this file is named internal because i want the same jsonrpc external api, but for machine learning purposes
http layer would slow down learning too much so jsonrpc will be just wrapper of this internal api
"""

from functools import lru_cache

import networkx as nx
from market_revisions_trader.services.binance_market.api import \
    buy as binance_buy
from market_revisions_trader.services.binance_market.client import get_client

binance_client = get_client()


class CannotBuyException(Exception):
    pass


class InvalidQuantity(Exception):
    pass

@lru_cache(maxsize=1)
def get_base_quote_digraph():
    """
    returns graph representing trading possibilities -
    can be also used for calculating prices of currency in terms of other currency
    """
    symbols = list(
        filter(
            lambda symbol: symbol['status'] == 'TRADING' and 'MARKET' in symbol['orderTypes'],
            binance_client.get_exchange_info()['symbols']))
    DG = nx.DiGraph()
    for symbol in symbols:
        DG.add_node(symbol['baseAsset'])
        DG.add_node(symbol['quoteAsset'])
        DG.add_edge(symbol['baseAsset'], symbol['quoteAsset'], fee=0.001, via='base_quote')
        DG.add_edge(symbol['quoteAsset'], symbol['baseAsset'], fee=0.001, via='quote_base')
    return DG

@lru_cache(maxsize=10000)
def can_buy(base, quote):
    try:
        return nx.shortest_path(get_base_quote_digraph(), source=base, target=quote, weight='fee')
    except nx.NetworkXNoPath:
        return False

def buy(base=None, quote=None, quantity=None, marketid='binance'):
    canbuy = can_buy(base, quote)
    if canbuy is False:
        raise CannotBuyException
    if quantity is None or quantity <= 0:
        raise InvalidQuantity

    if marketid == 'binance':
        return binance_buy(base=base, quote=quote, quantity=quantity, path=canbuy, graph=get_base_quote_digraph())

    # TODO virtual market buy
