from binance.client import Client
from market_revisions_trader.services.binance_market.config import BINANCE_API_KEY, BINANCE_SECRET_KEY

def get_client():
    return Client(BINANCE_API_KEY, BINANCE_SECRET_KEY)
