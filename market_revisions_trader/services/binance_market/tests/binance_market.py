import unittest

from market_revisions_trader.services.binance_market.client import get_client
from pyramid import testing
from market_revisions_trader.services.binance_market.api import get_tickers


class BinanceMarketTests(unittest.TestCase):

    def setUp(self):
        self.config = testing.setUp()
        self.binance_client = get_client()

    def tearDown(self):
        testing.tearDown()

    def test_get_klines(self):
        klines = self.binance_client.get_klines(symbol='BTCUSDT', interval=self.binance_client.KLINE_INTERVAL_1MINUTE)
        assert isinstance(klines, list)

    def test_get_symbol_info(self):
        info = self.binance_client.get_symbol_info('BTCUSDT')
        assert info['baseAsset'] == 'BTC'
        assert info['quoteAsset'] == 'USDT'

    def test_dummy_digraph_tutorial(self):
        import networkx as nx
        DG = nx.DiGraph()
        DG.add_node('BTC')
        DG.add_node('ETH')
        DG.add_edge('BTC', 'ETH', fee=0.1)
        DG.add_node('ADA')
        DG.add_edge('BTC', 'ADA', fee=0.01)
        DG.add_edge('ADA', 'ETH', fee=0.01)

        path = nx.shortest_path(DG, source='BTC', target='ETH', weight='fee')
        assert ['BTC', 'ADA', 'ETH'] == path
        try:
            nx.shortest_path(DG, 'ETH', 'BTC')
            assert False
        except nx.NetworkXNoPath:
            pass

    def test_get_tickers(self):
        tickers = get_tickers()
        raise Exception(tickers)





