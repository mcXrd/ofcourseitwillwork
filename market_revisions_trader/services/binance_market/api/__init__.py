"""
This module should provide bridge to binance for trading_api
"""


from functools import lru_cache

from market_revisions_trader.services.binance_market.client import get_client

binance_client = get_client()

@lru_cache(maxsize=1) # TODO - add timeout
def get_tickers():
    return binance_client.get_all_tickers()

def to_usdt(symbol=None, symbol_quantity=1):
    pass
    # TODO

def from_usdt(symbol=None, usdt_quantity=1):
    pass
    # TODO

def buy(base=None, quote=None, quantity=None, path=None, graph=None):
    """
    this should trade anything to anything - just check the expected fees via trading_api can_buy function
    TODO - need to actually test it after implementing to_usdt and from_usdt
    """
    target_usdt_value = to_usdt(symbol=quote, quote_quantity=quantity)
    _last_node = None
    for node in path:
        if _last_node is None:
            _last_node = node
            continue

        via = graph.edges[_last_node, node]['via']
        side = binance_client.SIDE_BUY
        _quantity = from_usdt(symbol=node, usdt_quantity=target_usdt_value)
        if via =='quote_base':
            side = binance_client.SIDE_SELL

        order = binance_client.create_order(
            newOrderRespType='FULL',
            symbol=_last_node+node,
            side=side,
            type=binance_client.ORDER_TYPE_MARKET,
            quantity=_quantity)
        assert order['status']=='FILLED'
    return True
