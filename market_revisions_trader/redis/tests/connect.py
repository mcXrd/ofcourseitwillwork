import unittest

from pyramid import testing
from market_revisions_trader.redis.client import get_client


class RedisConnectTests(unittest.TestCase):

    def setUp(self):
        self.config = testing.setUp()
        self.redis_client = get_client()

    def tearDown(self):
        testing.tearDown()

    def test_redis_works(self):
        self.redis_client.set('test:holahola', 'data1')
        assert self.redis_client.get('test:holahola') == b'data1'
