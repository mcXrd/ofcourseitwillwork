for LINE in $(git diff-tree --no-commit-id --name-only -r HEAD) $(git ls-files -m)
do
	if [ $( echo "$LINE" | grep '\.py') ] && [ -f $LINE ]
	then
		if [ $( echo "$LINE" | grep '__init__.py') ]
		then
			uuid_name="$(uuidgen).py"
		    ln -s $LINE $uuid_name
		    sudo isort $uuid_name
		    unlink $uuid_name
		else
		    sudo isort $LINE
		fi
		if [ $(stat --format '%a' $LINE) != 664 ]
		then
			sudo chmod 664 $LINE
		fi
	fi
done
echo "Done"